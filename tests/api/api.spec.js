import { expect } from 'chai'
import { describe, it } from 'mocha'
import request from 'supertest-as-promised'
import app from '../test-server'

import { GOOGLE } from '../helpers/mocks'

describe('REST API tests', () => {
  describe('REST endpoints', () => {
    it('retrieves data at: /company/:uuid', async () => {
      let expected = GOOGLE.name
      let res = await request(app)
              .get(`/company/${GOOGLE.uuid}`)
              .expect(200)
      let result = JSON.parse(res.text)

      expect(result[0].properties.name).to.deep.equal(expected)

      await request(app)
              .get(`/company/${GOOGLE.uuid}`)
              .expect(200)
              .then(res => { expect(result[0].properties.name).to.deep.equal(expected) })
              .catch(err => { expect(err).to.not.exist })
    })

    // 3 companies in database as of now
    // TODO: use mock/test database and insert record first
    it('retrieves data at endpoint: /companies', async (done) => {
      let res = await request(app)
              .get('/companies')
              .expect(200)

      expect(JSON.parse(res.text).length).to.be.gte(1)

      await request(app)
              .get('/companies')
              .expect(200)
              .then(res => { expect(JSON.parse(res.text).length).to.be.gte(1) })
              .catch(err => { expect(err).to.not.exist })

      done()
    })

    it('random', (done) => {
      expect(true).to.be.true
      expect('200').to.equal('200')
      done()
    })
  })
})
