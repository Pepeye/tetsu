import uuid from 'node-uuid'

let newUUID = uuid.v4()
export let UUID = newUUID

export let TESTCOMPANY = {
  uuid: UUID,
  name: 'Example Inc',
  description: 'Example Inc is a test company',
  test: true,
  website: 'http://www.test.com'
}

export let TESTSEGMENT = {
  uuid: uuid.v4(),
  name: 'Example Segment',
  description: 'Example Segment is a test business',
  test: true,
  website: 'http://www.test.com'
}

export const GOOGLE = {
  'uuid': '0d03adef-e7c0-11e5-8687-ca0e2421946f',
  'name': 'Google',
  'description': `Google is an American multinational corporation specializing in Internet-related services and products. These include online advertising technologies, search, cloud computing, and software. Most of its profits are derived from AdWords, an online advertising service that places advertising near the list of search results.\nGoogle was founded by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University. Together they own about 14 percent of its shares but control 56 percent of the stockholder voting power through supervoting stock. They incorporated Google as a privately held company on September 4, 1998. An initial public offering followed on August 19, 2004. Its mission statement from the outset was \'to organize the world's information and make it universally accessible and useful,\' and its unofficial slogan was \'Don't be evil.\' In 2004, Google moved to its new headquarters in Mountain View, California, nicknamed the Googleplex.\nRapid growth since incorporation has triggered a chain of products, acquisitions and partnerships beyond Google's core search engine.`,
  'createdAt': null,
  'updatedAt': null,
  'website': 'http://www.google.com/',
  'mid': '/m/045c7b',
  'wikipedia': 'Google',
  'wiki': 1092923,
  'freebase': 'http://rdf.freebase.com/ns/m/045c7b',
  'basekb': 'http://rdf.basekb.com/ns/m.045c7b'
}

export const APPLE = {
  'uuid': '0c88d82e-e7c0-11e5-8687-ca0e2421946f',
  'name': 'Apple Inc.',
  'description': `Apple Inc. is an American multinational corporation headquartered in Cupertino, California, that designs, develops, and sells consumer electronics, computer software, online services, and personal computers. Its best-known hardware products are the Mac line of computers, the iPod media player, the iPhone smartphone, the iPad tablet computer, and the Apple Watch smartwatch. Its online services include iCloud, the iTunes Store, and the App Store. Apple's consumer software includes the OS X and iOS operating systems, the iTunes media browser, the Safari web browser, and the iLife and iWork creativity and productivity suites.\nApple was founded by Steve Jobs, Steve Wozniak, and Ronald Wayne on April 1, 1976, to develop and sell personal computers. It was incorporated as Apple Computer, Inc. on January 3, 1977, and was renamed as Apple Inc. on January 9, 2007, to reflect its shifted focus towards consumer electronics. Apple joined the Dow Jones Industrial Average on March 19, 2015.\nApple is the world's second-largest information technology company by revenue after Samsung Electronics, and the world's third-largest mobile phone maker.`,
  'createdAt': null,
  'updatedAt': null,
  'website': 'http://www.apple.com/',
  'mid': '/m/0k8z',
  'wikipedia': 'Apple_Inc$002E',
  'wiki': 856,
  'freebase': 'http://rdf.freebase.com/ns/m/0k8z',
  'basekb': 'http://rdf.basekb.com/ns/m.0k8z'
}

export const MICROSOFT = {
  'uuid': '0a82cee6-e7c0-11e5-8687-ca0e2421946f',
  'name': 'Microsoft Corporation',
  'description': `Microsoft Corporation is an American multinational corporation headquartered in Redmond, Washington, that develops, manufactures, licenses, supports and sells computer software, consumer electronics and personal computers and services. Its best known software products are the Microsoft Windows line of operating systems, Microsoft Office office suite, and Internet Explorer web browser. Its flagship hardware products are the Xbox game consoles and the Microsoft Surface tablet lineup. It is the world's largest software maker measured by revenues. It is also one of the world's most valuable companies.\nMicrosoft was founded by Bill Gates and Paul Allen on April 4, 1975, to develop and sell BASIC interpreters for Altair 8800. It rose to dominate the personal computer operating system market with MS-DOS in the mid-1980s, followed by Microsoft Windows. The company's 1986 initial public offering, and subsequent rise in its share price, created three billionaires and an estimated 12,000 millionaires from Microsoft employees. Since the 1990s, it has increasingly diversified from the operating system market and has made a number of corporate acquisitions.`,
  'createdAt': null,
  'updatedAt': null,
  'website': 'http://www.microsoft.com',
  'mid': '/m/04sv4',
  'wikipedia': 'Microsoft',
  'wiki': 19001,
  'freebase': 'http://rdf.freebase.com/ns/m/04sv4',
  'basekb': 'http://rdf.basekb.com/ns/m.04sv4'
}

export const genUUID = () => uuid.v4()
