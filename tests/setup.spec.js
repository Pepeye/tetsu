import Node from '../src/graphql/modules/node/model'
import Edge from '../src/graphql/modules/edge/model'
import { TESTCOMPANY, TESTSEGMENT, genUUID } from './helpers/mocks'

before(() => {
  let props = {
    uuid: genUUID(),
    kind: 'SUBJECT'
  }
  Node.create(TESTCOMPANY)
  Node.create(TESTSEGMENT)
  Edge.create(TESTCOMPANY.uuid, TESTSEGMENT.uuid, 'OWNS', props)
})

after(() => {
  Node.query('MATCH (n { test: true } ) DETACH DELETE n')
})
