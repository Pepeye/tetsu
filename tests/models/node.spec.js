import { expect } from 'chai'
import { describe, it } from 'mocha'
import Node from '../../src/graphql/modules/node/model'
import { UUID, TESTCOMPANY, TESTSEGMENT, genUUID } from '../helpers/mocks'
import { validateNeo4jNode } from '../helpers/helpers'

// S P E C S

describe('Node tests', () => {
  describe('Node:', () => {
    let newnode = new Node(TESTCOMPANY)

    it('CREATES a new node with labels', async () => {
      return await Node
        .create(TESTCOMPANY)
        .then(result => {
          validateNeo4jNode(result)
        })
        .catch(err => { expect(err).to.not.exist })
    })

    it('FINDS a node by UUID', async () => {
      await Node
        .find(UUID)
        .then(result => {
          validateNeo4jNode(result)
          // expect(result[0]).to.deep.equal(TESTCOMPANY)
        })
        .catch(err => { expect(err).to.not.exist })
    })

    it('QUERY nodes using CYPHER ', async () => {
      let qry = `
        MATCH (n)
        RETURN n
        LIMIT 2
      `
      await Node
        .query(qry)
        .then(result => {
          // let count = Object.keys(result).length
          expect(result).to.exist
          // expect(count).to.equal(2)  // TODO: remove later, fine with seed data
        })
        .catch(err => { expect(err).to.not.exist })
    })

    it('FINDS a node BY specified PROPERTIES', async () => {
      let params = { name: TESTCOMPANY.name, website: TESTCOMPANY.website }
      await Node
        .findByProperties(params)
        .then(result => {
          let count = Object.keys(result).length
          expect(result).to.exist
          expect(count).to.equal(1) // TODO: remove later, fine with seed data
        })
        .catch(err => { expect(err).to.not.exist })
    })

    it('GETS node UUID property on class', () => {
      // let newnode = new Company(TESTCOMPANY)
      expect(newnode.uuid).to.equal(UUID)
    })

    it('GETS node LABELS property on class', () => {
      // let newnode = new Company(TESTCOMPANY)
      // let labels = newnode.label
      expect(newnode.label).to.deep.equal([])
    })

    it('SETS node LABELS property on class', () => {
      // let newnode = new Company(TESTCOMPANY)
      newnode.label = ['Company', 'Business']
      expect(newnode.label).to.deep.equal(['Company', 'Business'])
    })

    it('GETS node PROPERTIES', () => {
      // let newnode = new Company(TESTCOMPANY)
      expect(newnode.info).to.deep.equal(TESTCOMPANY)
    })

    it('SETS node PROPERTIES on class', () => {
      // let newnode = new Company(TESTCOMPANY)
      let newprops = {
        uuid: UUID,
        name: 'Example Inc',
        description: 'Example Inc is a test company',
        test: true,
        website: 'http://www.test.com'
      }
      newnode.info = newprops
      expect(newnode.info).to.deep.equal(newprops)
      // expect(newnode.info).to.deep.equal(newnode.properties)
    })

    it('can SAVE node PROPERTIES to DB', async () => {
      let uuid = genUUID()
      // let newnode = new Company(TESTCOMPANY)
      let newprops = {
        name: 'Example Inc',
        description: 'Example Inc is a test company',
        test: true,
        website: 'http://www.test.com'
      }
      newnode.info = newprops
      await newnode
        .save()
        .then(saved => {
          Node
            .findByProperties({uuid})
            .then(result => {
              expect(result).to.deep.equal(saved)
            })
            .catch(err => { expect(err).to.not.exist })
        })
    })

    it('can UPDATE node PROPERTIES to DB', async () => {
      let props = {
        name: 'Updated Example Company',
        description: 'Updated example description',
        test: true,
        website: 'http://www.example.com',
        updated: true
      }

      await Node
        .update(UUID, props)
        .then(result => {
          expect(result[0].properties.updated).to.be.true
        })
        .catch(err => { expect(err).to.not.exist })
    })

    it('ADDS to node LABELS in DB', async () => {
      await Node
        .addLabels(newnode.uuid, ['Thing'])
        .then(result => {
          expect(result[0].labels.indexOf('Thing')).to.be.gt(-1)
        })
        .catch(err => { expect(err).to.not.exist })
    })

    it('GETS node LABELS from DB', async () => {
      await Node
        .findLabels(UUID)
        .then(result => {
          expect(result[0]).to.exist
          expect(result[0].indexOf('Thing')).to.be.gt(-1)
        })
        .catch(err => { expect(err).to.not.exist })
    })

    it('can REMOVE label from node', async () => {
      // let newnode = new Company(TESTCOMPANY)
      await Node
        .deleteLabels(UUID, ['Business'])
        .then(result => {
          expect(result).to.exist
          expect(result[0].labels).to.deep.equal(['Company', 'Thing'])
          validateNeo4jNode(result)
        })
        .catch(err => { expect(err).to.not.exist })
    })

    it('DELETES a node', async () => {
      let msg = { status: 200, message: `Node (uuid: ${newnode.uuid}) successfully deleted` }
      await Node
        .delete(newnode.uuid)
        .then(data => {
          expect(data).to.exist
          expect(data).to.deep.equal(msg)
        })
        .catch(err => {
          console.log('[error]', err)
          expect(err).to.not.exist
        })
    })

    // TODO: don't use hard coded uuid or setup test data first
    it('CREATES EDGE on a node', async () => {
      let uuid = genUUID()
      let props = {
        uuid,
        stake: 1.0,
        kind: 'SUBJECT'
      }

      await Node
        .createEdge(TESTCOMPANY.uuid, TESTSEGMENT.uuid, 'OWNS', props)
        .then(result => {
          expect(result).to.exist
        })
        .catch(err => {
          expect(err).to.not.exist
        })
    })

    it('FINDS EDGES on a node', async () => {
      await Node
        .findEdges(TESTCOMPANY.uuid)
        .then(result => {
          expect(result).to.exist
        })
        .catch(err => { expect(err).to.not.exist })
    })

    it('DELETES EDGE on a node', async () => {
      let props = {
        kind: 'SUBJECT'
      }

      await Node
        .deleteEdge(TESTCOMPANY.uuid, TESTSEGMENT.uuid, 'OWNS', props)
        .then(result => {
          expect(result).to.exist
          expect(result.status).to.equal(200)
        })
        .catch(err => {
          console.log('[error]', err)
          expect(err).to.not.exist
        })
    })
  })
})
