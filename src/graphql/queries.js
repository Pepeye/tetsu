import { GraphQLObjectType } from 'graphql'

// import module queries
import company from './modules/company/queries'
import sector from './modules/sector/queries'
import stock from './modules/stock/queries'
import exchange from './modules/exchange/queries'

// const rootFields = Object.assign(company)
const rootFields = {
  ...company,
  ...sector,
  ...stock,
  ...exchange
}

export default new GraphQLObjectType({
  name: 'Query',
  fields: () => rootFields
})
