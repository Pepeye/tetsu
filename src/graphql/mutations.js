import {GraphQLObjectType} from 'graphql'
// TODO: import module mutations

const rootFields = Object.assign({})

export default new GraphQLObjectType({
  name: 'Mutation',
  fields: () => rootFields
})
