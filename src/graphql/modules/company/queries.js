import {
  // GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull
} from 'graphql'

import { GraphQLCompany } from './schema'
import Company from './model'

export default {
  getCompanyByID: {
    description: 'a graph node of type company',
    type: new GraphQLList(GraphQLCompany),
    args: {
      uuid: { type: new GraphQLNonNull(GraphQLString) }
    },
    resolve: (root, { uuid }) => Company.find(uuid)
  },

  getCompanies: {
    description: 'a collection of companies',
    type: new GraphQLList(GraphQLCompany),
    resolve: (root, args) => Company.all(['Company'])
  }
}
