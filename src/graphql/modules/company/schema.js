import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLList,
  GraphQLInt,
  GraphQLFloat,
  GraphQLNonNull
} from 'graphql'

import { GraphQLSector } from '../sector/schema'
import { GraphQLStock } from '../stock/schema'

import Company from './model'

export const GraphQLCompany = new GraphQLObjectType({
  name: 'Company',
  fields: () => ({
    _id: { type: new GraphQLNonNull(GraphQLID), description: 'ID of the company node' },
    labels: { type: new GraphQLList(GraphQLString), description: 'Company node labels' },
    properties: { type: CompanyProps, description: 'Company node properties' },
    sectors: {
      type: new GraphQLList(GraphQLSector),
      description: 'Company sector classifications',
      resolve: ({ properties, labels }, args) => {
        let company = new Company(properties, labels)
        return company.sectors()
      }
    },
    stocks: {
      type: new GraphQLList(GraphQLStock),
      description: 'List of company stock issues',
      resolve: ({ properties, labels }, args) => {
        let company = new Company(properties, labels)
        return company.stocks()
      }
    }
  })
})

const CompanyProps = new GraphQLObjectType({
  name: 'CompanyProperties',
  description: 'The company node properties',
  fields: () => ({
    uuid: { type: GraphQLString, description: `Company id` },
    name: { type: GraphQLString, description: `Company name` },
    description: { type: GraphQLString, description: `The description of the Company` },
    createdAt: { type: GraphQLFloat, description: `The UTC datetime the Company was created` },
    updatedAt: { type: GraphQLFloat, description: 'The UTC datetime the Company was last updated' },
    website: { type: GraphQLString, description: 'Website address of the company' },
    mid: { type: GraphQLString, description: `Freebase machine-generated id (mids) of the Company` },
    wikipedia: { type: GraphQLString, description: `Company wikipedia topic identification string` },
    wiki: { type: GraphQLInt, description: `Company wikipedia pageid or curid` },
    freebase: { type: GraphQLString, description: `Freebase uri of Company` },
    basekb: { type: GraphQLString, description: `Freebase uri of Company as found in basekb AWS instance` }
  })
})

// export default GraphQLCompany
