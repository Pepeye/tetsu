import { execute } from '../../../lib/db'
import Node from '../node/model'

class Company extends Node {

  constructor (props, labels = ['Company']) {
    super(props, labels)
  }

  async sectors () {
    let params = { uuid: this.uuid }
    let query = `
      MATCH (c:Company {uuid: { uuid } } )-[r:CLASSIFIED]-(s:Sector)
      RETURN s
    `

    let result = await execute({ query, params })
    return result.map(row => row.s)
  }

  async stocks () {
    let params = { uuid: this.uuid }
    let query = `
      MATCH (c:Company {uuid: { uuid } } )-[r:ISSUED]-(s:Stock)
      RETURN s
    `

    let result = await execute({ query, params })
    return result.map(row => row.s)
  }

}

export default Company
