import { execute } from '../../../lib/db'
import Node from '../node/model'

class Stock extends Node {

  constructor (props, labels = ['Stock']) {
    super(props, labels)
  }

  async issuer () {
    let params = { uuid: this.uuid }
    let query = `
      MATCH (s:Stock {uuid: { uuid } } )<-[r:ISSUED]-(c:Company)
      RETURN c
    `

    let result = await execute({ query, params })
    return result.map(row => row.c)
  }

  async exchange () {
    let params = { uuid: this.uuid }
    let query = `
      MATCH (s:Stock {uuid: { uuid } } )-[r:LISTED_ON]->(e:Exchange)
      RETURN e
    `

    let result = await execute({ query, params })
    return result.map(row => row.e)
  }

}

export default Stock
