import {
  // GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull
} from 'graphql'

import { GraphQLStock } from './schema'
import Stock from './model'

export default {
  getStockByID: {
    description: 'a graph node of type Stock',
    type: new GraphQLList(GraphQLStock),
    args: {
      uuid: { type: new GraphQLNonNull(GraphQLString) }
    },
    resolve: (root, { uuid }) => Stock.find(uuid)
  },

  getStocks: {
    description: 'a collection of Stocks',
    type: new GraphQLList(GraphQLStock),
    resolve: (root, args) => Stock.all(['Stock'])
  }
}
