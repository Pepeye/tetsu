import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLList,
  GraphQLInt,
  GraphQLFloat,
  GraphQLNonNull
} from 'graphql'

import { GraphQLCompany } from '../company/schema'
import { GraphQLExchange } from '../exchange/schema'
import Stock from './model'

export const GraphQLStock = new GraphQLObjectType({
  name: 'GraphQLStock',
  fields: () => ({
    _id: { type: new GraphQLNonNull(GraphQLID), description: 'ID of the Stock node' },
    labels: { type: new GraphQLList(GraphQLString), description: 'Stock node labels' },
    properties: { type: StockProps, description: 'Stock node properties' },
    issuer: {
      type: new GraphQLList(GraphQLCompany),
      description: 'Sub-Stocks of current Stock',
      // resolve: ({ labels, properties }, args) => {
      resolve: (source, args) => {
        let { properties, labels } = source
        let stock = new Stock(properties, labels)
        return stock.issuer()
      }
    },
    exchange: {
      type: new GraphQLList(GraphQLExchange),
      description: 'Stock participants (companies active in Stock)',
      resolve: ({ properties, labels }, args) => {
        let stock = new Stock(properties, labels)
        return stock.exchange()
      }
    }
  })
})

const StockProps = new GraphQLObjectType({
  name: 'StockProperties',
  description: 'The Stock node properties',
  fields: () => ({
    uuid: { type: GraphQLString, description: `Stock id` },
    ticker: { type: GraphQLString, description: `Stock ticker` },
    symbol: { type: GraphQLString, description: `Stock symbol` },
    createdAt: { type: GraphQLFloat, description: `The UTC datetime the Stock was created` },
    updatedAt: { type: GraphQLFloat, description: 'The UTC datetime the Stock was last updated' },
    issue_type: { type: GraphQLString, description: 'Stock issue type' },
    mid: { type: GraphQLString, description: `Freebase machine-generated id (mids) of the Stock` },
    wikipedia: { type: GraphQLString, description: `Stock wikipedia topic identification string` },
    wiki: { type: GraphQLInt, description: `Stock wikipedia pageid or curid` },
    freebase: { type: GraphQLString, description: `Freebase uri of Stock` },
    basekb: { type: GraphQLString, description: `Freebase uri of Stock as found in basekb AWS instance` }
  })
})

// export default GraphQLStock
