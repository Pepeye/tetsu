import {
  // GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull
} from 'graphql'

import { GraphQLExchange } from './schema'
import Exchange from './model'

export default {
  getExchangeByID: {
    description: 'a graph node of type Exchange',
    type: new GraphQLList(GraphQLExchange),
    args: {
      uuid: { type: new GraphQLNonNull(GraphQLString) }
    },
    resolve: (root, { uuid }) => Exchange.find(uuid)
  },

  getExchanges: {
    description: 'a collection of Exchanges',
    type: new GraphQLList(GraphQLExchange),
    resolve: (root, args) => Exchange.all(['Exchange'])
  }
}
