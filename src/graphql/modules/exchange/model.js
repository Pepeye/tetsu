import { execute } from '../../../lib/db'
import Node from '../node/model'

class Exchange extends Node {

  constructor (props, labels = ['Exchange']) {
    super(props, labels)
  }

  async listings () {
    let params = { uuid: this.uuid }
    let query = `
      MATCH (e:Exchange {uuid: { uuid } } )<-[r:LISTED_ON]-(s:Stock)
      RETURN s
    `

    let result = await execute({ query, params })
    return result.map(row => row.c)
  }

  async issuers () {
    let params = { uuid: this.uuid }
    let query = `
    MATCH (e:Exchange {uuid: { uuid } } )<-[r:LISTED_ON]-(s:Stock)<-[:ISSUED]-(c:Company)
    RETURN c
    `

    let result = await execute({ query, params })
    return result.map(row => row.e)
  }

}

export default Exchange
