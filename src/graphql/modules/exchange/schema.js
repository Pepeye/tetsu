import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLList,
  GraphQLInt,
  GraphQLFloat,
  GraphQLNonNull
} from 'graphql'

import { GraphQLStock } from '../stock/schema'
import { GraphQLCompany } from '../company/schema'
import Exchange from './model'

export const GraphQLExchange = new GraphQLObjectType({
  name: 'GraphQLExchange',
  fields: () => ({
    _id: { type: new GraphQLNonNull(GraphQLID), description: 'ID of the company node' },
    labels: { type: new GraphQLList(GraphQLString), description: 'Exchange node labels' },
    properties: { type: ExchangeProps, description: 'Exchange node properties' },
    listings: {
      type: new GraphQLList(GraphQLStock),
      description: 'List of securities on the exchange',
      resolve: ({ properties, labels }, args) => {
        let exchange = new Exchange(properties, labels)
        return exchange.listings()
      }
    },
    issuers: {
      type: new GraphQLList(GraphQLCompany),
      description: 'List of issuers on the exchange',
      resolve: ({ properties, labels }, args) => {
        let exchange = new Exchange(properties, labels)
        return exchange.issuers()
      }
    }
  })
})

const ExchangeProps = new GraphQLObjectType({
  name: 'ExchangeProperties',
  description: 'The company node properties',
  fields: () => ({
    uuid: { type: GraphQLString, description: `Exchange id` },
    name: { type: GraphQLString, description: `Exchange name` },
    founded: { type: GraphQLString, description: `Date Exchange was founded` },
    description: { type: GraphQLString, description: `The description of the Exchange` },
    createdAt: { type: GraphQLFloat, description: `The UTC datetime the Exchange was created` },
    updatedAt: { type: GraphQLFloat, description: 'The UTC datetime the Exchange was last updated' },
    website: { type: GraphQLString, description: 'Website address of the company' },
    mid: { type: GraphQLString, description: `Freebase machine-generated id (mids) of the Exchange` },
    wikipedia: { type: GraphQLString, description: `Exchange wikipedia topic identification string` },
    wiki: { type: GraphQLInt, description: `Exchange wikipedia pageid or curid` },
    freebase: { type: GraphQLString, description: `Freebase uri of Exchange` },
    basekb: { type: GraphQLString, description: `Freebase uri of Exchange as found in basekb AWS instance` }
  })
})

// export default GraphQLCompany
