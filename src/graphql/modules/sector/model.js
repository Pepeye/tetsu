import { execute } from '../../../lib/db'
import Node from '../node/model'

class Sector extends Node {

  constructor (props, labels = ['Sector']) {
    super(props, labels)
  }

  async participants () {
    let params = { uuid: this.uuid }
    let query = `
      MATCH (c:Company)-[r:CLASSIFIED]->(s:Sector {uuid: { uuid } } )
      RETURN c
    `
    let result = await execute({ query, params })
    return result.map(row => row.c)
  }

  async subSectors () {
    let params = { uuid: this.uuid }
    let query = `
      MATCH (s:Sector {uuid: { uuid } } )<-[r:SUB_SECTOR]-(sub:Sector)
      RETURN sub
    `
    let result = await execute({ query, params })
    return result.map(row => row.sub)
  }

}

export default Sector
