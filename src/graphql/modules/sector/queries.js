import {
  // GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull
} from 'graphql'

import { GraphQLSector } from './schema'
import Sector from './model'

export default {
  getSectorByID: {
    description: 'a graph node of type sector',
    type: new GraphQLList(GraphQLSector),
    args: {
      uuid: { type: new GraphQLNonNull(GraphQLString) }
    },
    resolve: (root, { uuid }) => Sector.find(uuid)
  },

  getSectors: {
    description: 'a collection of sectors',
    type: new GraphQLList(GraphQLSector),
    resolve: (root, args) => Sector.all(['Sector'])
  }
}
