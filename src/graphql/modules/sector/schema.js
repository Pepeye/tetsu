import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLList,
  GraphQLInt,
  GraphQLFloat,
  GraphQLNonNull
} from 'graphql'

import { GraphQLCompany } from '../company/schema'
import Sector from './model'

export const GraphQLSector = new GraphQLObjectType({
  name: 'GraphQLSector',
  fields: () => ({
    _id: { type: new GraphQLNonNull(GraphQLID), description: 'ID of the Sector node' },
    labels: { type: new GraphQLList(GraphQLString), description: 'Sector node labels' },
    properties: { type: SectorProps, description: 'Sector node properties' },
    subs: {
      type: new GraphQLList(GraphQLSector),
      description: 'Sub-sectors of current sector',
      // resolve: ({ labels, properties }, args) => {
      resolve: (source, args) => {
        let { properties, labels } = source
        let sector = new Sector(properties, labels)
        return sector.subSectors()
      }
    },
    participants: {
      type: new GraphQLList(GraphQLCompany),
      description: 'Sector participants (companies active in sector)',
      resolve: ({ properties, labels }, args) => {
        let sector = new Sector(properties, labels)
        return sector.participants()
      }
    }
  })
})

const SectorProps = new GraphQLObjectType({
  name: 'SectorProperties',
  description: 'The Sector node properties',
  fields: () => ({
    uuid: { type: GraphQLString, description: `Sector id` },
    name: { type: GraphQLString, description: `Sector name` },
    description: { type: GraphQLString, description: `The description of the Sector` },
    createdAt: { type: GraphQLFloat, description: `The UTC datetime the Sector was created` },
    updatedAt: { type: GraphQLFloat, description: 'The UTC datetime the Sector was last updated' },
    website: { type: GraphQLString, description: 'Website address of the Sector' },
    mid: { type: GraphQLString, description: `Freebase machine-generated id (mids) of the Sector` },
    wikipedia: { type: GraphQLString, description: `Sector wikipedia topic identification string` },
    wiki: { type: GraphQLInt, description: `Sector wikipedia pageid or curid` },
    freebase: { type: GraphQLString, description: `Freebase uri of Sector` },
    basekb: { type: GraphQLString, description: `Freebase uri of Sector as found in basekb AWS instance` }
  })
})

// export default GraphQLSector
