import {
  GraphQLString,
  GraphQLInt,
  GraphQLFloat
} from 'graphql'

export const baseProperties = (model) => {
  return {
    uuid: { type: GraphQLString, description: `${model} id` },
    name: { type: GraphQLString, description: `${model} name` },
    description: { type: GraphQLString, description: `The description of the ${model}` },
    createdAt: { type: GraphQLFloat, description: `The UTC datetime the ${model} was created` },
    updatedAt: { type: GraphQLFloat, description: 'The UTC datetime the ${model} was last updated' }
  }
}

export const linkedProperties = (model) => {
  return {
    mid: { type: GraphQLString, description: `Freebase machine-generated id ("mids") of the ${model}` },
    wikipedia: { type: GraphQLString, description: `${model} wikipedia topic identification string - http://en.wikipedia.org/wiki/TOPIC_STRING` },
    wiki: { type: GraphQLInt, description: `${model} wikipedia pageid or curid - http://en.wikipedia.org?curid=WIKI_ID` },
    freebase: { type: GraphQLString, description: `Freebase uri of ${model}` },
    basekb: { type: GraphQLString, description: `Freebase uri of ${model} as found in basekb's AWS instance` }
  }
}
